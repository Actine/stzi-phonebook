package info.teib.phonebook.phonebook;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Клас-фасад для телефонної книги
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class PhoneBook {

    private SortedSet<Contact> contacts;
    private List<Contact> contactsList;         // cached for quick retrieval
    private int lastId = 0;

    public PhoneBook() {
        contacts = new TreeSet<Contact>(new ContactAlphabeticalComparator());
    }

    public int getCount() {
        return contacts.size();
    }

    public List<Contact> getContacts() {
        if (contactsList == null) {
            contactsList = new ArrayList<Contact>(contacts);
        }
        return contactsList;
    }

    public List<Contact> getContacts(int limit, int offset) {
        final int size = Math.min(contacts.size() - offset, limit);
        if (size <= 0) {
            return Collections.emptyList();
        }

        List<Contact> all = getContacts();
        List<Contact> filtered = new ArrayList<Contact>(size);
        for (int i = offset, end = offset + size; i < end; i++) {
            filtered.add(all.get(i));
        }
        return filtered;
    }

    public List<Contact> findContacts(@NotNull String search) {
        final String lowercaseSearch = search.toLowerCase();
        List<Contact> found = new ArrayList<Contact>();
        for (Contact contact : contacts) {
            if (contact.getName().toLowerCase().contains(lowercaseSearch)) {
                found.add(contact);
            }
        }
        return found;
    }

    public boolean addContact(@NotNull Contact contact) {
        contactsList = null;
        contact.id = lastId++;
        return contacts.add(contact);
    }

    public boolean deleteContact(@NotNull Contact contact) {
        contactsList = null;
        return contacts.remove(contact);
    }

    public List<Contact> getSortedByBirthday() {
        List<Contact> sorted = new ArrayList<Contact>(contacts);
        Collections.sort(sorted, new ContactDateComparator(getToday()));
        return sorted;
    }

    public List<Contact> getContactsWithBirthdayToday() {
        List<Contact> filtered = new ArrayList<Contact>();
        Date today = getToday().getTime();
        for (Contact contact : contacts) {
            if (today.equals(contact.getDateOfBirth())) {
                filtered.add(contact);
            }
        }
        return filtered;
    }

    private GregorianCalendar getToday() {
        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static String prettyPrintContacts(Collection<Contact> contacts) {
        StringBuilder builder = new StringBuilder(contacts.size() * 80 + 30);
        int i = 1;
        for (Contact contact : contacts) {
            builder
                    .append("------------------------------\n")
                    .append(i++).append(". ").append(contact.getName())
                    .append('\n');

            final Set<Contact.PhoneEntry> phoneEntries = contact.getPhoneEntries();
            if (phoneEntries.isEmpty()) {
                builder.append("номер телефону відсутній\n");
            } else {
                final int phonesSize = phoneEntries.size();
                Contact.PhoneEntry firstEntry = phoneEntries.iterator().next();
                builder.append(firstEntry.prettyPrint());
                if (phonesSize > 1) {
                    builder.append(String.format(" + ще %d%n", phonesSize - 1));
                } else {
                    builder.append('\n');
                }
            }
        }
        builder.append("------------------------------\n");
        return builder.toString();
    }

    private static class ContactAlphabeticalComparator implements Comparator<Contact> {
        public int compare(Contact o1, Contact o2) {
            int result = o1.getName().compareToIgnoreCase(o2.getName());
            return result != 0 ? result : o1.id - o2.id;
        }
    }

    private static class ContactDateComparator implements Comparator<Contact> {
        private Calendar today;
        private Calendar calendar1;
        private Calendar calendar2;

        public ContactDateComparator(Calendar today) {
            this.today = today;
            calendar1 = new GregorianCalendar();
            calendar2 = new GregorianCalendar();
        }

        public int compare(Contact o1, Contact o2) {
            final Date dateOfBirth1 = o1.getDateOfBirth();
            final Date dateOfBirth2 = o2.getDateOfBirth();
            if (dateOfBirth1 == null && dateOfBirth2 == null) {
                return 0;
            } else if (dateOfBirth1 == null) {
                return 1;
            } else if (dateOfBirth2 == null) {
                return -1;
            }

            calendar1.setTime(dateOfBirth1);
            calendar2.setTime(dateOfBirth2);
            calendar1.set(Calendar.YEAR, today.get(Calendar.YEAR));
            calendar2.set(Calendar.YEAR, today.get(Calendar.YEAR));
            if (calendar1.before(today)) {
                calendar1.roll(Calendar.YEAR, true);
            }
            if (calendar2.before(today)) {
                calendar1.roll(Calendar.YEAR, true);
            }
            return calendar1.compareTo(calendar2);
        }
    }

}
