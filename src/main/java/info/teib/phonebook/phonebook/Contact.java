package info.teib.phonebook.phonebook;

import com.sun.istack.internal.NotNull;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Картка контакту
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class Contact {

    int id;
    private String name;
    private Set<PhoneEntry> phoneEntries;
    private String address;
    private String email;
    private String im;
    private Date dateOfBirth;

    private static DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);

    public Contact(@NotNull String name) {
        this.name = name;
        this.phoneEntries = new LinkedHashSet<PhoneEntry>();
    }

    /**
     * Клонуючий конструктор
     * @param proto Контакт для клонування
     */
    public Contact(Contact proto) {
        this.name = proto.name;
        this.phoneEntries = new LinkedHashSet<PhoneEntry>(proto.phoneEntries);
        this.address = proto.address;
        this.email = proto.email;
        this.im = proto.im;
        this.dateOfBirth = new Date(proto.dateOfBirth.getTime());
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    public Set<PhoneEntry> getPhoneEntries() {
        return phoneEntries;
    }

    public void addPhoneEntry(PhoneEntry entry) {
        phoneEntries.add(entry);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setDateOfBirth(int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year, month, day);
        this.dateOfBirth = calendar.getTime();
    }

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Contact contact = (Contact) o;

        if (!name.equals(contact.name)) { return false; }
        if (phoneEntries != null ? !phoneEntries.equals(contact.phoneEntries) : contact.phoneEntries != null) {
            return false;
        }
        if (address != null ? !address.equals(contact.address) : contact.address != null) { return false; }
        if (email != null ? !email.equals(contact.email) : contact.email != null) { return false; }
        if (im != null ? !im.equals(contact.im) : contact.im != null) { return false; }
        return !(dateOfBirth != null ? !dateOfBirth.equals(contact.dateOfBirth) : contact.dateOfBirth != null);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (phoneEntries != null ? phoneEntries.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (im != null ? im.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", phoneEntries=" + phoneEntries +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", im='" + im + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }

    public String prettyPrint() {
        // Спершу збудуємо гарний рядок для виводу телефонів
        StringBuilder phones = new StringBuilder(phoneEntries.size() * 24);
        for (PhoneEntry entry : phoneEntries) {
            phones.append(entry.prettyPrint()).append('\n');
        }

        // Тепер виведемо гарну картку контакту
        return String.format("%s%n"
                        + "-----------------------------------------------%n"
                        + "%s"
                        + "-----------------------------------------------%n"
                        + "Адреса:          %s%n"
                        + "E-mail:          %s%n"
                        + "Месенджер:       %s%n"
                        + "День народження: %s%n",
                name,
                phones.toString(),
                address != null ? address : '-',
                email != null ? email : '-',
                im != null ? im : '-',
                dateOfBirth != null ? DATE_FORMAT.format(dateOfBirth) : '-'
        );
    }

    public static class PhoneEntry {

        private String number;
        private PhoneType type;

        public PhoneEntry(@NotNull String number, @NotNull PhoneType type) {
            this.number = number;
            this.type = type;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(@NotNull String number) {
            this.number = number;
        }

        public PhoneType getType() {
            return type;
        }

        public void setType(@NotNull PhoneType type) {
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            PhoneEntry that = (PhoneEntry) o;
            return number.equals(that.number) && type == that.type;
        }

        @Override
        public int hashCode() {
            int result = number.hashCode();
            result = 31 * result + type.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "PhoneEntry{" +
                    "number='" + number + '\'' +
                    ", type=" + type +
                    '}';
        }

        public String prettyPrint() {
            switch (type) {
                case HOME:
                    return number + " (дом.)";
                case MOBILE:
                    return number + " (моб.)";
                case WORK:
                    return number + " (роб.)";
                case FAX:
                    return number + " (факс)";
                default:
                    throw new IllegalArgumentException("WTF");
            }
        }
    }

    public enum PhoneType {
        HOME, MOBILE, WORK, FAX;
    }

}
