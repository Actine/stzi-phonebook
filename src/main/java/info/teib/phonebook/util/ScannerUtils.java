package info.teib.phonebook.util;

import java.util.Scanner;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public final class ScannerUtils {

    private ScannerUtils() {}

    public static String readOptional(Scanner scanner, String fallback) {
        String read = scanner.nextLine();
        return read.isEmpty() ? fallback : read;
    }

}
