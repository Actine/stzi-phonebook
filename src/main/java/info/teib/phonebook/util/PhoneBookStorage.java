package info.teib.phonebook.util;

import info.teib.phonebook.phonebook.Contact;
import info.teib.phonebook.phonebook.PhoneBook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * ����, ���� ������ ����� � ���� �� ����� �� �� �����
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class PhoneBookStorage {

    private File file;

    private static PhoneBookStorage instance;

    public static PhoneBookStorage get(File file) {
        if (instance == null || !instance.file.equals(file)) {
            instance = new PhoneBookStorage(file);
        }
        return instance;
    }

    private PhoneBookStorage(File file) {
        this.file = file;
    }

    public PhoneBook load() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        PhoneBook book = new PhoneBook();
        final int size = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < size; i++) {
            String name = scanner.nextLine();
            Contact contact = new Contact(name);
            int phones = Integer.parseInt(scanner.nextLine());
            for (int j = 0; j < phones; j++) {
                String number = scanner.nextLine();
                String type = scanner.nextLine();
                Contact.PhoneType typeEnum = "home".equals(type) ? Contact.PhoneType.HOME
                        : "mobile".equals(type) ? Contact.PhoneType.MOBILE
                        : "work".equals(type) ? Contact.PhoneType.WORK
                        : Contact.PhoneType.FAX;
                Contact.PhoneEntry entry = new Contact.PhoneEntry(number, typeEnum);
                contact.addPhoneEntry(entry);
            }
            String address = scanner.nextLine();
            if (!"null".equals(address)) {
                contact.setAddress(address.replace("\\n", "\n"));
            }
            String email = scanner.nextLine();
            if (!"null".equals(email)) {
                contact.setEmail(email);
            }
            String im = scanner.nextLine();
            if (!"null".equals(im)) {
                contact.setIm(im);
            }
            long dob = Long.parseLong(scanner.nextLine());
            if (dob != -1) {
                contact.setDateOfBirth(new Date(dob));
            }
            book.addContact(contact);
        }
        return book;
    }

    public void save(PhoneBook book) {
        PrintWriter printer = null;
        try {
            printer = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        final List<Contact> contacts = book.getContacts();
        final int size = contacts.size();
        printer.println(size);
        for (int i = 0; i < size; i++) {
            Contact contact = contacts.get(i);
            printer.println(contact.getName());
            final Set<Contact.PhoneEntry> entries = contact.getPhoneEntries();
            printer.println(entries.size());
            for (Contact.PhoneEntry e : entries) {
                printer.println(e.getNumber());
                switch (e.getType()) {
                    case HOME:
                        printer.println("home");
                        break;
                    case MOBILE:
                        printer.println("mobile");
                        break;
                    case WORK:
                        printer.println("work");
                        break;
                    case FAX:
                        printer.println("fax");
                        break;
                }
            }
            if (contact.getAddress() != null) {
                printer.println(contact.getAddress().replace("\n", "\\n"));
            } else {
                printer.println((Object) null);
            }
            printer.println(contact.getEmail());
            printer.println(contact.getIm());
            if (contact.getDateOfBirth() != null) {
                printer.println(contact.getDateOfBirth().getTime());
            } else {
                printer.println(-1);
            }
        }
        printer.close();
    }

}
