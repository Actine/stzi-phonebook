package info.teib.phonebook;

import info.teib.phonebook.phonebook.Contact;
import info.teib.phonebook.phonebook.PhoneBook;
import info.teib.phonebook.util.ContactGenerator;
import info.teib.phonebook.util.PhoneBookStorage;
import info.teib.phonebook.util.ScannerUtils;

import java.io.File;
import java.util.List;
import java.util.Scanner;

/**
 * CLI
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class Main {

    public static final String PHONEBOOK_FILE = "D:/phonebook.dat";
    public static final int PAGE_LIMIT = 5;

    private static PhoneBook phoneBook;
    private static Scanner scanner;
    private static Screen nextScreen;
    private static int contactId;
    private static int offset;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);

        // На якому екрані програми ми зараз (меню, перегляд контактів тощо)
        nextScreen = Screen.LOADING;

        while (true) {
            switch (nextScreen) {
                case LOADING:
                    showLoading();
                    break;
                case MAIN_MENU:
                    showMainMenu();
                    break;
                case LIST_ALL:
                    showListAll();
                    break;
                case LIST_PAGINATED:
                    showListPaginated();
                    break;
                case SAVE_AND_EXIT:
                    PhoneBookStorage.get(new File(PHONEBOOK_FILE)).save(phoneBook);
                    System.out.println("Контакти збережено. Гарного дня!");
                    return;
            }
        }

    }

    private static void showLoading() {
        System.out.println("Вітаємо в телефонній книзі\n"
                        + "Оберіть дію:\n"
                        + "1 - завантажити із файлу " + PHONEBOOK_FILE + '\n'
                        + "2 - згенерувати нову телефонну книгу"
        );

        int choice = scanner.nextInt();
        scanner.nextLine();
        if (choice == 1) {
            PhoneBookStorage phoneBookStorage = PhoneBookStorage.get(new File(PHONEBOOK_FILE));
            phoneBook = phoneBookStorage.load();
            System.out.println("Завантажено " + phoneBook.getCount() + " контактів.");

            nextScreen = Screen.MAIN_MENU;
        } else if (choice == 2) {
            System.out.println("Скільки контактів згенерувати? (100)");
            int count = Integer.parseInt(ScannerUtils.readOptional(scanner, "100"));
            phoneBook = new PhoneBook();
            ContactGenerator.fillPhoneBook(phoneBook, count);
            System.out.println(phoneBook.getCount() + " контактів згенеровано.");

            nextScreen = Screen.MAIN_MENU;
        }
    }

    private static void showMainMenu() {
        System.out.println("ГОЛОВНЕ МЕНЮ\n"
                        + "============\n\n"
                        + "Оберіть дію:\n"
                        + "1 - відобразити усі контакти одним списком\n"
                        + "2 - відобразити контакти сторінками по 5 записів\n"
                        + "3 - пошук контактів\n"
                        + "4 - додати контакт\n"
                        + "5 - переглянути найближчі дні народження\n"
                        + "0 - зберегти усі зміни і вийти"
        );

        int choice = scanner.nextInt();
        scanner.nextLine();
        switch (choice) {
            case 1:
                nextScreen = Screen.LIST_ALL;
                return;
            case 2:
                nextScreen = Screen.LIST_PAGINATED;
                return;
            case 3:
                nextScreen = Screen.SEARCH;
                return;
            case 4:
                nextScreen = Screen.ADD_CONTACT;
                return;
            case 5:
                nextScreen = Screen.LIST_BIRTHDAYS;
            case 0:
                nextScreen = Screen.SAVE_AND_EXIT;
        }
    }

    public static void showListAll() {
        final List<Contact> allContacts = phoneBook.getContacts();
        System.out.println("СПИСОК КОНТАКТІВ\n"
                        + "================\n\n"
                        + PhoneBook.prettyPrintContacts(allContacts) + '\n'
                        + "Оберіть дію:\n"
                        + "<номер п/п> - переглянути контакт\n"
                        + "Enter       - назад до меню"
        );

        String choice = ScannerUtils.readOptional(scanner, null);
        if (choice == null) {
            nextScreen = Screen.MAIN_MENU;
        } else {
            nextScreen = Screen.VIEW_CONTACT;
            contactId = allContacts.get(Integer.parseInt(choice)).getId();
        }
    }

    public static void showListPaginated() {
        final List<Contact> contacts = phoneBook.getContacts(PAGE_LIMIT, offset);
        System.out.println("СПИСОК КОНТАКТІВ\n"
                        + "================\n\n"
                        + "Контакти " + (offset + 1) + "-" + (offset + PAGE_LIMIT) + ":\n"
                        + PhoneBook.prettyPrintContacts(contacts) + '\n'
                        + "Оберіть дію:\n"
                        + "<номер п/п> - переглянути контакт\n"
                        + "  <         - попередні " + PAGE_LIMIT + '\n'
                        + "  >         - наступні " + PAGE_LIMIT + '\n'
                        + "Enter       - назад до меню"
        );

        String choice = ScannerUtils.readOptional(scanner, null);
        if (choice == null) {
            nextScreen = Screen.MAIN_MENU;
        } else if ("<".equals(choice)) {
            offset = Math.max(offset - PAGE_LIMIT, 0);
        } else if (">".equals(choice)) {
            if (offset + PAGE_LIMIT < phoneBook.getCount()) {
                offset += PAGE_LIMIT;
            }
        } else {
            nextScreen = Screen.VIEW_CONTACT;
            contactId = contacts.get(Integer.parseInt(choice)).getId();
        }
    }

    private enum Screen {
        LOADING, MAIN_MENU, LIST_ALL, LIST_PAGINATED, LIST_BIRTHDAYS, SEARCH, VIEW_CONTACT, ADD_CONTACT, SAVE_AND_EXIT
    }

}
